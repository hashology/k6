import { check, group, sleep } from "k6";
import http from "k6/http";

import jsonpath from "https://jslib.k6.io/jsonpath/1.0.2/index.js";
import { URL } from "https://jslib.k6.io/url/1.0.0/index.js";


const HOSTNAME = "<REDACTED>";
const CLIENT_ID =
  "<REDACTED>";
const CLIENT_SECRET =
  "<REDACTED>";
const LOGIN_USERNAME = "perf_testing";
const LOGIN_PASSWORD = "perfTesting1";

export const options = {
  stages: [{ duration: "1m", target: 100 }, {duration: "5m", target: 500 }, { duration: "15m", target: 50 }, { duration: "5m", target: 500 }, { duration: "1m", target: 100 }],
  //vus: 1,
  //iterations: 1,
  thresholds: {
    http_req_failed: [{ threshold: "rate<=10", abortOnFail: true }],
  },
};


function genRand(min, max, decimalPlaces) {  
  var rand = Math.random()*(max-min) + min;
  var power = Math.pow(10, decimalPlaces);
  return Math.floor(rand*power) / power;
}

export default function () {
  
  group("Get and Update Request", function () {
    let response;
    const vars = {};

    // Get Access Token
    response = http.post(
      `${HOSTNAME}/oauth/token?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&grant_type=client_credentials`
    );

    vars["access_token"] = jsonpath.query(response.json(), "$.access_token")[0];

    // Get Requests
    response = http.get(`${HOSTNAME}/api/rest/v1/grant_request?all=1&`, {
      headers: {
        Authorization: `Bearer ${vars["access_token"]}`,
      },
    });

    vars["request_ids"] = jsonpath.query(
      response.json(),
      "$..grant_request[:25].id"
    );

    let recommended = genRand(1, 1000000, 2);

    //TODO: returns 200 status code but the body includes and error message (wrong use of HTTP status codes)
    response = http.put(
      `${HOSTNAME}/api/rest/v1/grant_request/${
        vars["request_ids"][Math.floor(Math.random() * vars["request_ids"])]
      }`,
      {
        data: JSON.stringify({
          amount_recommended: recommended,
          amount_recommended_local: recommended,
          project_summary: "Perf Test",
          amount_requested: recommended,
          amount_requested_local: recommended,
        }),
      },
      {
        headers: {
          Authorization: `Bearer ${vars["access_token"]}`,
        },
      }
    );

    check(response, {
      "is status 200": (r) => r.status === 200,
    });
  });

  group("Filter Request", function () {
    let response;
    const vars = {};

    // Get Access Token
    response = http.post(
      `${HOSTNAME}/oauth/token?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&grant_type=client_credentials`
    );
    vars["access_token"] = jsonpath.query(response.json(), "$.access_token")[0];

    let random_amount = genRand(1, 100000, 2);

    response = http.post(
      `${HOSTNAME}/api/rest/v1/request_transaction/list`,
      {
        filter: JSON.stringify({
          "group_type": "or",
          "conditions": [
            [
              "amount_due",
              "lt",
              random_amount,
            ]
          ]
        }),
        style: "full"
      },
      {
        headers: {
          Authorization: `Bearer ${vars["access_token"]}`,
        },
      }
    );

    check(response, {
      "is status 200": (r) => r.status === 200,
    });
  });

  group("Get Request Reports", function () {
    let response;
    const vars = {};

    // Get Access Token
    response = http.post(
      `${HOSTNAME}/oauth/token?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&grant_type=client_credentials`
    );
    vars["access_token"] = jsonpath.query(response.json(), "$.access_token")[0];
    
    response = http.get(
      `${HOSTNAME}/api/rest/v1/request_report`,
      {
        headers: {
          Authorization: `Bearer ${vars["access_token"]}`,
        },
      }
    );

    check(response, {
      "is status 200": (r) => r.status === 200,
    });
  });

  group("Add/Remove Cards", function () {
    let response;
    const vars = {};

    //Get CSRF
    response = http.get(`${HOSTNAME}/`);

    vars["csrf_token"] = response
      .html()
      .find("meta[name='csrf-token']")
      .first()
      .attr("content");

    check(response, {
      "Get CSRF Token": (r) => vars["csrf_token"] != "",
    });

    //Login
    response = http.post(`${HOSTNAME}/user_sessions`, {
      authenticity_token: vars["csrf_token"],
      "user_session[portal]": "1",
      "user_session[login]": LOGIN_USERNAME,
      "user_session[password]": LOGIN_PASSWORD,
    });

    check(response, {
      "Login to Site": (r) => r.url.includes("/dashboard/index"),
    });

    let DB_URL = new URL(`${HOSTNAME}/client_stores.json/`);
    DB_URL.searchParams.append("authenticity_token", vars["csrf_token"]);
    DB_URL.searchParams.append("client_store_type", "dashboard");

    response = http.get(DB_URL.toString(), {
      headers: {
        "X-CSRF-Token": vars["csrf_token"],
      },
    });

    check(response, {
      "Load Dashboard": (r) => r.url.includes("/client_stores.json"),
    });

    vars["client_store_id"] = jsonpath.query(
      response.json(),
      "$[0].client_store.id"
    );
    response = http.put(
      `${HOSTNAME}/client_stores/${vars["client_store_id"]}`,
      {
        "client_store[name]": "Default",
        "client_store[client_store_type]": "dashboard",
        "client_store[data]": JSON.stringify({
          cards: [
            {
              uid: 11,
              title: "Requests",
              listing: {
                url: "/grant_requests",
                data: [],
              },
              viz: {},
              detail: {},
              settings: {
                minimized: false,
                emailNotifications: false,
                plugins: [],
                iconStyle: "style-grant-requests",
                modelTypeLabel: "Requests",
              },
            },
            {
              uid: 1,
              title: "Signals",
              listing: {
                url: "/posts",
                data: [],
              },
              viz: {},
              detail: {},
              settings: {
                minimized: true,
                emailNotifications: false,
                plugins: ["popoutCard"],
                iconStyle: "style-posts",
                modelTypeLabel: "Posts",
              },
            },
          ],
          nextUid: 12,
        }),
      },
      {
        headers: {
          "X-CSRF-Token": vars["csrf_token"],
        },
      }
    );
    check(response, {
      "Add Card": (r) => r.status == 201,
    });
    response = http.put(
      `${HOSTNAME}/client_stores/${vars["client_store_id"]}`,
      {
        "client_store[name]": "Default",
        "client_store[client_store_type]": "dashboard",
        "client_store[data]": JSON.stringify({
          cards: [
            {
              uid: 1,
              title: "Signals",
              listing: {
                url: "/posts",
                data: [],
              },
              viz: {},
              detail: {},
              settings: {
                minimized: true,
                emailNotifications: false,
                plugins: ["popoutCard"],
                iconStyle: "style-posts",
                modelTypeLabel: "Posts",
              },
            },
          ],
          nextUid: 12,
        }),
      },
      {
        headers: {
          "X-CSRF-Token": vars["csrf_token"],
        },
      }
    );
    check(response, {
      "Remove Card": (r) => r.status == 201,
    });
  });


  // Automatically added sleep
  sleep(Math.floor(Math.random() * 2) + 1);
}
